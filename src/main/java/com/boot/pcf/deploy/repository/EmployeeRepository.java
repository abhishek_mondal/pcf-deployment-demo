package com.boot.pcf.deploy.repository;

import com.boot.pcf.deploy.entity.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EmployeeRepository extends MongoRepository<Employee, Integer> {

}
