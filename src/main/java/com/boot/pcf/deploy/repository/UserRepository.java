package com.boot.pcf.deploy.repository;

import com.boot.pcf.deploy.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {

}
