package com.boot.pcf.deploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PCFDeploymentTestApplication {

	public static void main(String[] args) {

		SpringApplication.run(PCFDeploymentTestApplication.class, args);
	}

}
