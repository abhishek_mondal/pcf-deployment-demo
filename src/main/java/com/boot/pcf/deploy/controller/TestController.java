package com.boot.pcf.deploy.controller;

import com.boot.pcf.deploy.entity.Employee;
import com.boot.pcf.deploy.entity.User;
import com.boot.pcf.deploy.repository.EmployeeRepository;
import com.boot.pcf.deploy.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
public class TestController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Value("${WELCOME_MESSAGE}")
    private String message;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/healthCheck")
    public String testDeployment() {

        return message;
    }

    @PostMapping(path="/add") // Map ONLY POST Requests
    public @ResponseBody
    String addNewUser (@RequestParam String name
            , @RequestParam String email) {
        User n = new User();
        n.setName(name);
        n.setEmail(email);
        userRepository.save(n);
        return "User Data Saved Successfully";
    }

    @GetMapping(path="/all")
    public @ResponseBody Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Method to save employees in the db.
     * @param empList
     * @return
     */
    @PostMapping(value= "/create")
    public String create(@RequestBody List<Employee> empList) {
        logger.debug("Saving employees.");
        employeeRepository.saveAll(empList);
        return "Employee records created.";
    }

    /**
     * Method to fetch all employees from the db.
     * @return
     */
    @GetMapping(value= "/getAll")
    public Collection<Employee> getAll() {
        logger.debug("Getting all employees.");
        return employeeRepository.findAll();
    }

    /**
     * Method to fetch employee by id.
     * @param id
     * @return
     */
    @GetMapping(value= "/getbyid/{employee-id}")
    public Optional<Employee> getById(@PathVariable(value= "employee-id") int id) {
        logger.debug("Getting employee with employee-id= {}.", id);
        return employeeRepository.findById(id);
    }

    /**
     * Method to update employee by id.
     * @param id
     * @param emp
     * @return
     */
    @PutMapping(value= "/update/{employee-id}")
    public String update(@PathVariable(value= "employee-id") int id, @RequestBody Employee emp) {
        logger.debug("Updating employee with employee-id= {}.", id);
        emp.setId(id);
        employeeRepository.save(emp);
        return "Employee record for employee-id= " + id + " updated.";
    }

    /**
     * Method to delete employee by id.
     * @param id
     * @return
     */
    @DeleteMapping(value= "/delete/{employee-id}")
    public String delete(@PathVariable(value= "employee-id") int id) {
        logger.debug("Deleting employee with employee-id= {}.", id);
        employeeRepository.deleteById(id);
        return "Employee record for employee-id= " + id + " deleted.";
    }

    /**
     * Method to delete all employees from the db.
     * @return
     */
    @DeleteMapping(value= "/deleteAll")
    public String deleteAll() {
        logger.debug("Deleting all employees.");
        employeeRepository.deleteAll();
        return "All employee records deleted.";
    }

}
