This is a sample Spring Boot Project and below are the steps to deploy this application into PCF -

1. Register into Pivotal Portal (https://login.run.pivotal.io/login)
2. Create an Orgs in Pivotal portal and you can also create a custom space inside that Org
3. Install CF CLI into your local machine (brew install cloudfoundry/tap/cf-cli)
4. Login to Pivotal account from your Terminal/cmd using this command - 'cf login - a https://api.run.pivotal.io'
5. Choose your specific space in PCF to deploy your app.
6. Run 'cf push' command to push your application into PCF